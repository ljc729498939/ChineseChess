import BasePiece from "./Pieces/BasePiece";
import Board from "./Board";
const { ccclass, property } = cc._decorator;

@ccclass
export default class TouchDot extends cc.Component {

    //棋盘实例
    board: Board = null;
    //棋子
    piece: BasePiece = null;
    //索引
    index: cc.Vec2 = new cc.Vec2(0, 0);
    //坐标
    pos: cc.Vec2 = new cc.Vec2(0, 0);
    //box宽度
    width: number = 0;
    //box高度
    height: number = 0;

    onLoad() {
        this.width = this.node.width;
        this.height = this.node.height;
        this.pos.x = this.node.position.x;
        this.pos.y = this.node.position.y;

        //添加触摸事件
        this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
    }

    onTouchEnd(event: cc.Event.EventTouch){
        //this.node.color = cc.Color.BLACK;
        //this.node.opacity = 255;
        this.board.onDotTouchEnd(this);
    }
}
