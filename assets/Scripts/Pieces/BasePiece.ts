import { PieceType, PlayerType } from "../GameDefine";
const { ccclass, property } = cc._decorator;

//棋子基类
@ccclass
export default class BasePiece extends cc.Component {

    initPos: cc.Vec2 = cc.v2(0, 0);
    pieceType: PieceType = null;
    playerType: PlayerType = null;

    onLoad() {

    }

    init() {
        this.node.setPosition(this.initPos);

    }
}
