
//棋子类型
enum PieceType{
    PT_KING = "k",//将/帅
    PT_MANDARIN = "a",//士/仕
    PT_ELEPHANT = "b",//象/相
    PT_ROOKS = "r",//车
    PT_KNIGHTS = "n",//马
    PT_CANNONS = "c",//炮
    PT_PAWNS = "p"//兵
}

//玩家类型
enum PlayerType{
    PLT_BLACK = "b",//黑方
    PLT_RED = "r"//红方
}

export {PieceType,PlayerType}
